from django.urls import path
from .views import *

app_name = 'idea'

urlpatterns = [
    path('', post_view, name='post-list'),
    path('like/', like_post, name='like-post'),
    path('add-idea/', add_idea, name='add_idea'),
]
